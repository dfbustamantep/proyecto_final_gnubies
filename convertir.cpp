#include<iostream>
using std::cout;
using std::cin;
using std::endl;

void lines(void){
		cout<<"----------------------------------------------------------------"<<endl;
}

class Convertir{
	private:
		/* arreglo estarico
		 char texto[30];
		 */
		 //arreglo dinamico
		 char * texto;
		 int tam;
	public:
		Convertir(int tam);
		
		~Convertir();
		
		void EntradaDatos(void);
		
		void ProcesoDatos(void);
		
		void SalidaDatos(void);
};

Convertir::Convertir(int tam){
			//cout<<"Constructor";
			texto=new char[tam];
			for(int i=0;i<30;i++){
				//los char se pueden tomar como digitos numericos
					texto[i]=0;
			}
}
		
Convertir::~Convertir(){
			//cout<<"Destructor";
}
		
void Convertir::EntradaDatos(void){
				cout<<"\tIngreso de datos"<<endl;
				//llamamos a la funcion lineas
				lines();
				//limpiamos el buffer
				cin.ignore();
				//ingreso cadena de texto con el metodo cin.getline
				cout<<"Ingrese una cadena de texto: ";
				cin.getline(texto,30);
				
				//impresion de la cadena de texto antes de que se transforme
				cout<<"La cadena de texto es: ";
				for(int i=0;i<30;i++){
					cout<<texto[i];
				}
				
				cout<<endl;
				lines();
				//llamamamos al metodo procesoDatos
				ProcesoDatos();
		}
		
void Convertir::ProcesoDatos(void){
	//aca transformamos la cadena segun el codigo ascci
				for(int i=0;i<30;i++){
			
					if(texto[i]>=65 and texto[i]<=90){
						texto[i]=texto[i]+32;
					}
					else if(texto[i]>=97 and texto[i]<=122){
						texto[i]=texto[i]-32;
					}
					
				}
				//lamammos al metodo SalidaDatos
				SalidaDatos();
}
		
void Convertir::SalidaDatos(void){
	//mostramos la cadena
			cout<<"La cadena converitda es: "<<endl;
			for(int i=0;i<30;i++){
				cout<<texto[i];
			}
			cout<<endl;
			lines();
		}

int main(){
	//pedimos el tamaño del arreglo para el texto  y se lo mandamos al constructor
	int tam=0;
	cout<<"Digite el tamanio de la cadena de texto: ";cin>>tam;
		lines();
	Convertir conv(tam);
	
	conv.EntradaDatos();
	return 0;
}
